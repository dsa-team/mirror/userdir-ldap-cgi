#!/usr/bin/python3

# check password quality using cracklib given a new password, optionally the
# old password, and a list of ldap/gecos words via stdin, each on a line by
# itself (send an empty line if you want to skip the old password check)

# Copyright (c) 2008 Peter Palfrader

from __future__ import print_function

import sys, tempfile, os, subprocess
import cracklib

def cleanup(dir):
  if not dir.startswith('/tmp/pwcheck-'):
    raise ValueError('cleanup got a weird dir to remove: ' + dir)
  for f in 'dict.hwm dict.pwd dict.pwi wordlist wordlist-cleaned'.split(' '):
    p = dir+'/'+f
    if os.path.exists(p):
      os.remove(p)
  if os.path.exists(dir):
    os.rmdir(dir)



crack_mkdict = None
crack_packer = None
for b in ("/usr/sbin/crack_mkdict", "/usr/sbin/cracklib-format"):
  if os.path.exists(b):
    crack_mkdict = b
    break
for b in ("/usr/sbin/crack_packer", "/usr/sbin/cracklib-packer"):
  if os.path.exists(b):
    crack_packer = b
    break
if crack_mkdict is None or crack_packer is None:
  print("Could not find crack formater or packer", file=sys.stderr)
  sys.exit(1)


newpass = sys.stdin.readline().strip()
oldpass = sys.stdin.readline().strip()
ldapwords = [line.strip() for line in sys.stdin.readlines()]

if oldpass == "":
  oldpass = None


cracklib.min_length = 11

# check against the default dictionary
try:
  cracklib.VeryFascistCheck(newpass, oldpass, '/var/cache/cracklib/cracklib_dict')
except ValueError as e:
  print(e)
  sys.exit(1)

# and against a dictionary created from the ldap info on this user
if len(ldapwords) > 0:
  # squeeze's cracklib-packer complains about '*' on input - it
  # says 'skipping line: 1'
  while '-' in ldapwords:
    ldapwords.remove('-')
  while '*' in ldapwords:
    ldapwords.remove('*')
  while '' in ldapwords:
    ldapwords.remove('')

  tmpdir = tempfile.mkdtemp('', 'pwcheck-')
  with open(tmpdir+'/wordlist', "w") as F:
    for w in ldapwords:
      print(w, file=F)
    for w1 in ldapwords:
      for w2 in ldapwords:
        print(w1 + w2, file=F)
        print(w1[0] + w2, file=F)
  with open(os.path.join(tmpdir, "wordlist-cleaned"), "w") as cleaned:
    mkdict = subprocess.Popen([crack_mkdict, os.path.join(tmpdir, "wordlist")], stdout=cleaned)
  if mkdict.wait():
    print("crack_mkdict returned non-zero exit status %d." % mkdict.returncode, file=sys.stderr)
    cleanup(tmpdir)
    sys.exit(1)
  with open(os.path.join(tmpdir, "wordlist-cleaned")) as cleaned, open(os.devnull, "w") as devnull:
    packer = subprocess.Popen([crack_packer, os.path.join(tmpdir, "dict")], stdin=cleaned, stdout=devnull)
  if packer.wait():
    print("crack_packer returned non-zero exit status %d." % packer.returncode, file=sys.stderr)
    cleanup(tmpdir)
    sys.exit(1)

  try:
    cracklib.VeryFascistCheck(newpass, None, tmpdir+"/dict")
  except ValueError as e:
    print("ldap data based check: %s" % e, file=sys.stderr)
    cleanup(tmpdir)
    sys.exit(1)

  cleanup(tmpdir)

sys.exit(0)
