#use wml::db.d.o title="Email Forwarding"
#use wml::vbar

<dsatoc/>

<p>
Emails to @debian.org addresses go through an LDAP distributed email system. 
This system uses the forwarding field in the LDAP directory to route mail 
without passing it through a user's .forward file on a single host.
Multiple machines participate in the forwarding to provide redundancy.

<p>
Each forwarder inspects the LDAP database
to determine the forwarding address for foo@debian.org. The <i>envelope
to address</i> is rewritten and the message redirected to the new address.
As that redirection occurs on the mail relays, there is no opportunity for
the use of .forward files, procmail or other filtering. Extension addresses
(foo-lists) are supported, but the extension will not be preserved when
forwarding - i.e. if <tt>foo@debian.org</tt> redirects to <tt>foo@example.com</tt>,
then <tt>foo-lists@debian.org</tt> also redirects to <tt>foo@example.com</tt>.

<p>
As a special-case, the forwarding address may be foo@master.debian.org,
in which case the message is relayed to that system for processing by the
user's .forward or .procmailrc files. Forwarding to master.debian.org preserves
the extension part of the original address.

<p>
All machines also use the forwarding attribute as a default destination for
email. If the user has a home directory and no .forward file the mail is
forwarded rather than delivered to /var/spool/mail. This makes sure cron
reports, bug responses and other unexpected emails are not misplaced.

<p>
If you set the forwarding address to be a specific Debian machine and do 
not create a forward file then that machine will spool the mail to 
/var/spool/mail instead of creating a mail loop.

<p>
The email forwarding can be easily reconfigured using GnuPG:
<pre>
echo "emailforward: foo@bar.com" | gpg --clearsign | mail changes@db.debian.org
</pre>
or by visiting <a href="https://db.debian.org/login.html">db.debian.org</a>

<p>
You can test the email routing by using the command
<tt>/usr/sbin/exim -bt foo@debian.org</tt>

<h2>procmail</h2>
If you use procmail for your main mailbox, PLEASE, erase your .forward
file and put a .procmailrc in its place instead.
.procmailrc files will not be synchronised to all hosts in
the LDAP directory, so you will need to make sure the file exists on any
relevant hosts yourself.

<p>
The correct way to invoke procmail for extension addresses is "|/usr/bin/procmail [options]"
Ignore the IFS=".." stuff in the procmail man page.

<h2>Mailbox formats</h2>
Email can be saved to mailboxes or maildirs by using appropriately formatted lines in a
.forward file:
<p>
Mailbox format files "/home/foo/Mbox"
<br>Maildir format files "/home/foo/MailDir/"

<p>
To deliver to /var/spool/mail/foo use a construct like '|/usr/bin/procmail
-m /dev/null'. Putting the mailbox path will not work. You must use
absolute paths for mailboxes, qmail-like ./ paths are not supported by
Exim.

<p>
Also, 'Exim Filter' files are deliberately turned off.

<h2>Spam handling</h2>
<p>
Debian developers have a wide variety of loud and conflicting opinions
about what constitutes correct handling of their mail, making it impossible for
an admin to choose a single setup that fits all use cases.
</p>

<p>
Instead, we invite you to configure your own spam handling.
</p>

<p>
Some options available to you are:
</p>
<ul>
  <li><b>emailForward</b> Address to forward your mail to.  Setting this and
  then rejecting mail from d.o machines is less than helpful.
  <li><b>mailCallout</b> Whether or not to use Sender Address Verification.
  <li><b>mailContentInspectionAction</b> One of reject, markup, or blackhole.
  Applies to checks done on the content of message bodies, such as spam and
  virus checks.  Reject is default, and will reject the mail if a match occurs.
  Markup will add a header and then forward the mail to you anyway.  Blackhole
  will accept the mail and silently discard it.
  <li><b>mailDefaultOptions</b> Whether to enable the 'normal' set of
  SMTP time checks that DSA decide are appropriate.  Currently includes greylisting
  and some RBLs.  Defaults to true.
  <li><b>mailGreylisting</b> Whether to enable greylisting.
  <li><b>mailRBL</b> Set of RBLs to use.
  <li><b>mailRHSBL</b> Set of RHSBLs to use.
  <li><b>mailWhitelist</b> Sender envelopes to whitelist.
  <li><b>mailDisableMessage</b> Absolute last resort measure - will disable
  incoming mail from all machines not part of the Debian host list (see
  /var/lib/misc/thishost/debianhosts on any d.o machine).  This makes it very
  difficult for things like automated pings and mass mailings to all concerned
  DDs about changes to happen, and is strongly discouraged.
</ul>
