#!/usr/bin/perl -wT

# $Id: fetchkey.cgi,v 1.4 2004/11/18 14:23:05 joey Exp $
# (c) 1999 Randolph Chung. Licensed under the GPL. <tausq@debian.org>

use lib '.';
use strict;
use CGI;
use Util;

# Global settings...
my %config = &Util::ReadConfigFile;
for my $key (keys %config) {
    $key =~ /(.*)/;
    $config{$key} = $1;
}

my $query = new CGI;
print "Content-type: text/plain\n\n";

my $fp = $query->param('fingerprint');
$fp =~ /^([A-Fa-f0-9]+)$/;
$fp = $1;

if ($fp) {
  my $key = &Util::FetchKey($fp, 0);
  if ($key) {
    print $key;
  } else {
    print "Sorry, no key found matching fingerprint $fp\n";
  }
} else {
  print "No fingerprint given\n";
}

exit 0;

